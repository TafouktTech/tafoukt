// console.log(Meteor.settings)
if (Meteor.settings.sample && Meteor.settings.sample.users && Meteor.users.find().count() === 0) {
	console.log('Creating sample users')
	admin = Accounts.createUser({
		username: 'admin',
		password: 'password',
		email: 'admin@tafoukt.com',
		profile: {
			name: 'Tafoukt Admin'
		}
	});

	Meteor.users.update(admin, {$set: {
		admin: true,
		stuff: true,
		customer: true
	}});

	stuff = Accounts.createUser({
		username: 'stuff',
		password: 'password',
		email: 'stuff@tafoukt.com',
		profile: {
			name: 'Tafoukt Stuff'
		}
	});

	Meteor.users.update(stuff, {$set: {
		admin: false,
		stuff: true,
		customer: true
	}});

	customer = Accounts.createUser({
		username: 'customer',
		password: 'password',
		email: 'customer@tafoukt.com',
		profile: {
			name: 'Tafoukt Customer'
		}
	});

	Meteor.users.update(customer, {$set: {
		admin: false,
		stuff: false,
		customer: true
	}});

	Meteor._debug('Sample users created.')
};

Meteor.startup(function () {
	Meteor.publish(null, function () {
		if (! this.userId) {
			return null
		} else if(Tafoukt.is(this.userId, "admin")) {
			return Meteor.users.find({}, {fields: {
					admin: 1, 
					customer: 1, 
					stuff: 1, 
					profile: 1, 
					createdAt: 1,
					username: 1
				}})
		} else {
			return Meteor.users.find({_id: this.userId}, {fields: {
					admin: 1, 
					customer: 1, 
					stuff: 1, 
					profile: 1, 
					createdAt: 1,
					username: 1,
					services: 1
				}})
		}
	});
});

// Pages = new Meteor.Collection(null);

// Meteor.publish('tafoukt-pages', function () {
// 	return return Pages.find();
// })