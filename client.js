if (typeof Tafoukt === "undefined"){
	Tafoukt = {};
};
Tafoukt.keys = {};

// Tafoukt.ready
Tafoukt.keys.READY = "tafoukt-ready";
Session.set(Tafoukt.keys.READY, false);
Meteor.startup(function () { if (GPages.find().count()) Session.set(Tafoukt.keys.READY, true); });
Tafoukt.pagesHandler = Meteor.subscribe('tafoukt-pages', {
	onReady: function () { Session.set(Tafoukt.keys.READY, true); }
});
// if we have our pages on localtorage
// or if the suscription to pages is ready
Tafoukt.ready = function () {
	return Session.get(Tafoukt.keys.READY);
};


// Collections
// Tafoukt.Store = GroundDB
Tafoukt.Users = Users = Meteor.users;
// Tafoukt.Roles = Roles = Meteor.roles;
Tafoukt.Pages = Pages = Tafoukt.Collection('tafoukt-pages');
// Tafoukt.GPages = GPages = new GroundDB(Pages);
// Tafoukt.Facts = Facts = Tafoukt.Collection('facts');
Tafoukt.Contents = Contents = Tafoukt.Collection('tafoukt-contents');

// Models
var me = Meteor.uuid();
Tafoukt.me = function () {
	return Meteor.userId() || me ;
};


// var Page = function (page) {
// 	var self = this;
// 	self.layout = page.layout;
// 	self.routes = page.routes;
// };

// _.extend(Page.prototype, {
// });


// Tafoukt.Page = Page;