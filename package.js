Package.describe({
  summary: "Tafoukt?",
  version: "0.0.1"
});

Package.on_use(function (api, where) {
  api.imply(['accounts-password', 'accounts-base']);
  api.use([
    'meteor', 'underscore', 'livedata', 'minimongo', 'mongo-livedata', 'deps', 
    'session', 'webapp', 'accounts-base',

    'streams', 'user-status', 
    // 'grounddb'
  ], ['client', 'server']);
  api.add_files('common.js', ['client', 'server']);
  api.add_files('client.js', 'client');
  api.add_files('server.js', 'server');
  api.export([
    "Tafoukt",
    "tafoukt"
  ], ['client', 'server']);
});

Package.on_test(function (api) {
  api.use('tafoukt');
  api.use('tinytest');
  api.add_files('tafoukt_tests.js', ['client', 'server']);
});
