if (typeof Tafoukt === "undefined") {
	Tafoukt = {};
}
// Meteor.Streams
// Thanks Arunoda https://github.com/arunoda/meteor-streams/
Tafoukt.Stream = Meteor.Stream;

// Tafoukt.Collection(name)
// return a new Meteor.Collection(name) or return already created collection
var collections = Tafoukt.collections = {};
Tafoukt.Collection = function ( collectionName ) {
	if( _.has( collections, collectionName )) {
		return collections[ collectionName ];
	} else {
		collections[ collectionName ] = new Meteor.Collection( collectionName );
		return collections[ collectionName ];
	}
};

// Tafoukt.permessions
Tafoukt.permessions = [
	"admin",
	"stuff",
	"customer" 
];

function checkUser (user, is) {
	return _.result(user, is);
};

function findAndCheckUser (id, is) {
	var user = Meteor.users.findOne({_id: id});
	return user && checkUser(user, is);
};

Tafoukt.is = function (userOrUserId, whatever) {
	if (_.isString(userOrUserId))
		return findAndCheckUser(userOrUserId, whatever);
	else if (_.isObject(userOrUserId))
		return checkUser(userOrUserId, whatever);
	else
		return null;
};

